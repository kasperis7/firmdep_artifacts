## firmdep artifacts

- 固件处理脚本
    - https://gitlab.com/firmdep/img-gen-scripts
    - makeimage.sh 用于生成虚拟机镜像，输入为通过firmadyne extractor提取的`.tar.gz`格式的rootfs
    - fiximage.sh rootfs预修复，fork于 https://github.com/firmadyne/firmadyne/blob/master/scripts/fixImage.sh
    - 24329.tar.gz 测试用固件

- PANDA(包含tracer插件) 
    - https://gitlab.com/firmdep/PANDA
    - fork于 https://github.com/panda-re/panda

- API劫持库 
    - 仓库：https://gitlab.com/firmdep/libnvram
    - fork于 https://github.com/firmadyne/libnvram

- FirmDep仿真环境相关的其他文件（预编译的API劫持库、内核、initramfs、PANDA需要的额外文件）
    - https://gitlab.com/firmdep/misc

- 脚本（PANDA启动脚本和执行轨迹分析脚本）
    - 仓库：https://gitlab.com/firmdep/scripts
    - 文档整理中，近期更新

- FirmDep运行环境docker镜像：近期更新

- dataset.xls 固件数据集信息
